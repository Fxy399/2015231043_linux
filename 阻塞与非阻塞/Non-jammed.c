#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
void sigint_handler(int sig)
 {
    printf("received SIGINT signal successed!\n");
    return;
 }
void main() 
{
   pid_t pc;
   pc = fork();
   if(pc == 0)
 {  
   printf("The first child process with pid %d\n",getpid());
   sleep(3);
   printf("The second child process with pid %d\n",getpid());
   sleep(3);
   printf("The third child process with pid %d\n",getpid());
   _exit;
  } else 
  if(pc > 0) 
 {
  signal(SIGINT,sigint_handler);
  pause();
 } else
  if(pc < 0)
 {
  printf("fork process is error!\n");
  _exit;
 }
}
